# SAMx4

# (C) 2023 Ciaran Anscomb
#
# Released under the Creative Commons Attribution-ShareAlike 4.0
# International License (CC BY-SA 4.0).  Full text in the LICENSE file.

# Requires GNU Make.

PACKAGE_NAME = samx4
PACKAGE_VERSION = 1.2.1

CPLD_TARGETS = \
	cpld/samx4.jed cpld/samx4.svf \
	cpld/samx4-785.jed cpld/samx4-785.svf \
	cpld/samx4-rockyhill.jed cpld/samx4-rockyhill.svf \
	cpld/samx4-785-rockyhill.jed cpld/samx4-785-rockyhill.svf

.PHONY: all
all: $(CPLD_TARGETS)

###

$(CPLD_TARGETS):
	$(MAKE) -C cpld $(notdir $@)

###

distdir = $(PACKAGE_NAME)-$(PACKAGE_VERSION)

.PHONY: dist
dist: $(CPLD_TARGETS)
	@if test -e "$(distdir)"; then echo "$(distdir) exists: not creating zip" >&2; exit 1; fi
	ln -s . "$(distdir)"
	git archive --format=zip --prefix=$(distdir)/ -o $(distdir).zip HEAD
	zip $(distdir).zip $(foreach t,$(CPLD_TARGETS),$(distdir)/$(t))
	rm -f "$(distdir)"

###

.PHONY: clean
clean:
	$(MAKE) -C cpld clean

.PHONY: clena
clena: clean
	@echo "How much clena do you want it?"

.PHONY: distclean
distclean: clean
	$(MAKE) -C cpld distclean
