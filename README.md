---
title: 'SAMx4 - MC6883 replacement board'
author:
- Ciaran Anscomb
...

Intended to replace the functionality of the SN74LS783 Synchronous Address
Multiplexer (MC6883), and extend it to provide the same functionality as
Stewart Orchard's 256K banker board.

Released under the Creative Commons Attribution-ShareAlike 4.0 International
License (CC BY-SA 4.0).  Full text in the LICENSE file.

## PCB

`pcb/` contains KiCad schematic and PCB designs.

`pcb-rockyhill/` contains an alternative PCB design by Pedro Peña that is much
lower profile.  The pinout on the CPLD is changed, so you must be sure to build
with the right constraints file.

## CPLD

`cpld/` contains VHDL source for the Xilinx XC95144XL CPLD.  Either open the
`samx4.xise` project file in ISE or source the ISE environment files and run
`make` in this directory (GNU Make required).

There is a set of boolean generic constants near the top of the VHDL source
which configure whether support is built in for:

- 256K banker support
- 4K/16K ram sizes
- SN74LS785 behaviour
- Fast mode video[^1]

The SN74LS785 outputs different values for S in map type 1, and supports an
extra 16K x 4 DRAM type.

[^1]: When RAM is not being accessed, video data can be fetched instead.  When
    running mostly from ROM, this lets you see a fairly noisy picture.  Because
    this also enables some refresh, it breaks Stewart Orchard's refresh tester.

Note that for some configuration combinations you may need to enable
"exhaustive fit" in ISE.  I recommend that you do that rather than try
configuring ISE to optimise for "density", as the timings suffer.

These generic constants can be changed by:

- Editing the .vhd source file
- Modifying the "-generics" option in XST's process properties in ISE
- Overriding the XST\_GENERICS variable in the Makefile

The default targets for `make` use the last option to build '785 versions of
the CPLD programming files.

## Silly extra

If you run `make samx4-scroll` or `make samx4-785-scroll`, you can build a
version with finer-grained control over the video address.  The base address
(the 'F' register in the SAM) is extended to 11 bits, and a 5-bit "X offset"
register is added.  Modify with writes to:

```
    $FF3A - clear X offset register (Xoff, 5-bit)
    $FF3B - clear video base (F register, now 11-bit)
    $FF3C - left shift 0 into Xoff
    $FF3D - left shift 1 into Xoff
    $FF3E - left shift 0 into F
    $FF3F - left shift 1 into F

    Xoff added to lower VRAM address bits.
```

Support for 4K and 16K is omitted in these builds to make space.

## Acknowledgements

With thanks to Stewart Orchard for various notes on timing, and to both Stewart
and Pedro Peña for testing across a range of Dragon and Tandy Colour Computers.
